import Employee from './Employee.js';

class CommissionEmployee extends Employee {
  constructor(firstName, lastName, socialSecurityNumber, grossSales, commissionRate) {
    super(firstName, lastName, socialSecurityNumber);
    this.grossSales = grossSales;
    this.commissionRate = commissionRate;
  }
}

export default CommissionEmployee;
