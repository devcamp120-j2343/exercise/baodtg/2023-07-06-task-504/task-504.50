class Employee {
    constructor(firstName, lastName, socialSecurityNumber) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.socialSecurityNumber = socialSecurityNumber;
    }
  }
  
  export default Employee;
  